import warnings

from sqlalchemy import create_engine
from sqlalchemy.exc import SAWarning
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

db_path = '../ressources/AuroraDB.db'

engine = None

Session = None

Base = None

metadata = None

warnings.filterwarnings('ignore', r".*support Decimal objects natively", SAWarning, r'^sqlalchemy\.sql\.sqltypes$')


def connect(filepath: str = None):
    global db_path, engine, Session, Base, metadata

    if filepath:
        db_path = filepath

    engine = create_engine(db_path, echo=False)

    Session = sessionmaker(bind=engine)
    Base = declarative_base()
    metadata = Base.metadata


def get_session():
    return Session()
