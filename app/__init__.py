# Import flask and template operators
from flask import Flask

import database
from app.auth import get_auth

# Define the WSGI application object

app = Flask(__name__)
auth = get_auth()
# Configurations
app.config.from_object('config')

# Define the database object which is imported
# by modules and controllers
database.connect(app.config['SQLALCHEMY_DATABASE_URI'])

# Import a module / component using its blueprint handler variable (mod_auth)
from app.mod_user.controllers import user_bp

# Register blueprint(s)
app.register_blueprint(user_bp)
# app.register_blueprint(xyz_module)
# ..
