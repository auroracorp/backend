from flask import Blueprint, abort, jsonify, request
from sqlalchemy.orm.exc import NoResultFound

from app import app, auth
from app.mod_corporation.models import Corporation
from database import get_session

corporation_bp = Blueprint('corporation', __name__, url_prefix=f'/{app.config["API_VERSION"]}/corporation')


@corporation_bp.route('', methods=['PUT'])
def add_corporation():
    session = get_session()

    request_json = request.get_json()

    if ['name', 'description'] not in request_json.keys():
        abort(400)

    try:
        corp = Corporation(name=request_json['name'], password=request_json['description'])
        session.add(corp)
        session.commit()
        return "", 201
    except Exception as ex:
        abort(500)


@corporation_bp.route(f'/<int:corpid>', methods=['POST'])
@auth.login_required()
def mod_corporation(corpid):
    session = get_session()
    request_json = request.get_json()

    if not request_json:
        abort(400)

    try:
        corp = session.query(Corporation).filter(Corporation.id == corpid).one()

        if 'name' in request_json.keys():
            corp.name = request_json['name']

        if 'description' in request_json.keys():
            corp.password = request_json['description']

        session.commit()
        return "", 200
    except Exception as ex:
        abort(500)


@corporation_bp.route(f'/<int:corpid>', methods=['DELETE'])
@auth.login_required()
def del_corporation(corpid):
    session = get_session()

    try:
        session.query(Corporation).filter(Corporation.id == corpid).delete(synchronize_session=False)
        session.commit()
        return "", 200
    except Exception as ex:
        abort(500)


@corporation_bp.route(f'/<int:corpid>', methods=['GET'])
@auth.login_required()
def get_corporation(corpid):
    session = get_session()

    try:
        user = session.query(Corporation).filter(Corporation.id == corpid).one()
        return jsonify(user)
    except NoResultFound as notfound:
        abort(404)


@corporation_bp.route(f'', methods=['GET'])
@auth.login_required(role=['admin', 'corp-owner'])
def get_all_corporation_with_filter():
    session = get_session()

    name = request.args.get('name', default=None, type=str)

    query = session.query(Corporation)

    if name:
        query = query.filter(Corporation.name.like(f'%{name}%'))

    return jsonify(query.all())
