# coding: utf-8
from sqlalchemy import Column, Integer, Text, ForeignKey
from sqlalchemy.orm import relationship

from database import Base


class Corporations(Base):
    __tablename__ = 'corporations'

    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False, index=True)
    description = Column(Text)


class UsersCorporations(Base):
    __tablename__ = 'users_corporations'

    id = Column(Integer, primary_key=True)
    corp_id = Column(ForeignKey('corporations.id'), nullable=False)
    user_id = Column(ForeignKey('users.user_id'), nullable=False)
    corp_role = Column(Text, nullable=False)

    corp = relationship('Corporation')
    user = relationship('User')
