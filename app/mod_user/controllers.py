from flask import Blueprint, abort, jsonify, request
from sqlalchemy.orm.exc import NoResultFound

from app import app, auth
from app.mod_permission.models import UsersPermissions, Permissions
from app.mod_user.models import Users
from database import get_session

user_bp = Blueprint('user', __name__, url_prefix=f'/{app.config["API_VERSION"]}/user')


@user_bp.route('', methods=['PUT'])
def add_user():
    session = get_session()

    request_json = request.get_json()

    if not request_json['name'] or not request_json['password']:
        abort(400)

    try:
        user = Users(name=request_json['name'], password=request_json['password'])
        session.add(user)
        session.commit()
        return "", 201
    except Exception as ex:
        abort(500)


@user_bp.route(f'/<int:userid>', methods=['POST'])
@auth.login_required()
def mod_user(userid):
    session = get_session()
    request_json = request.get_json()

    if not request_json:
        abort(400)

    try:
        user = session.query(Users).filter(Users.user_id == userid).one()

        if 'name' in request_json.keys():
            user.name = request_json['name']

        if 'password' in request_json.keys():
            user.password = request_json['password']

        session.commit()
        return "", 200
    except Exception as ex:
        abort(500)


@user_bp.route(f'/<int:userid>', methods=['DELETE'])
@auth.login_required()
def del_user(userid):
    session = get_session()

    try:
        session.query(Users).filter(Users.user_id == userid).delete(synchronize_session=False)
        session.commit()
        return "", 200
    except Exception as ex:
        abort(500)


@user_bp.route(f'/<int:userid>', methods=['GET'])
#@auth.login_required()
def get_user(userid):
    session = get_session()

    try:
        user = session.query(Users).join(UsersPermissions) \
            .join(Permissions, UsersPermissions.permission_id == Permissions.id) \
            .filter(Users.id == userid).one()
        return jsonify(user)
    except NoResultFound as notfound:
        abort(404)


@user_bp.route(f'', methods=['GET'])
@auth.login_required(role=['admin', 'corp-owner'])
def get_all_with_filter():
    session = get_session()

    name = request.args.get('name', default=None, type=str)

    query = session.query(Users)

    if name:
        query = query.filter(Users.name.like(f'%{name}%'))

    return jsonify(query.all())
