# coding: utf-8
from sqlalchemy import Column, Integer, Text

from database import Base


class Users(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False, index=True)
    password = Column(Text, nullable=False, unique=True)

    def __init__(self, name=None, password=None):
        self.name = name
        self.password = password

    def from_json(self, json_data):
        pass

    def from_dict(self, dict_data: dict):
        for key, value in dict_data.items():
            if key in self.__dict__.keys():
                self.__dict__[key] = value

    def to_json(self):
        pass
