# coding: utf-8
from sqlalchemy import Column, Integer, Text, ForeignKey
from sqlalchemy.orm import relationship

from database import Base


class Permissions(Base):
    __tablename__ = 'permissions'

    id = Column(Integer, primary_key=True)
    role = Column(Text, nullable=False)
    description = Column(Integer)


class UsersPermissions(Base):
    __tablename__ = 'users_permissions'

    id = Column(Integer, primary_key=True)
    user_id = Column(ForeignKey('users.id'), nullable=False)
    permission_id = Column(ForeignKey('permissions.id'), nullable=False)

    permission = relationship('Permissions')
    user = relationship('Users')
