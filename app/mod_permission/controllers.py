from flask import Blueprint, abort, jsonify, request
from sqlalchemy.orm.exc import NoResultFound

from app import app, auth
from app.mod_permission.models import Permission
from app.mod_user.models import Users
from database import get_session

permission_bp = Blueprint('permission', __name__, url_prefix=f'/{app.config["API_VERSION"]}/permission')


@permission_bp.route('', methods=['PUT'])
@auth.login_required()
def add_permission():
    session = get_session()

    request_json = request.get_json()

    if not request_json['role'] or not request_json['description']:
        abort(400)

    try:
        permission = Permission(name=request_json['role'], password=request_json['description'])
        session.add(permission)
        session.commit()
        return "", 201
    except Exception as ex:
        abort(500)


@permission_bp.route(f'/<int:permissionid>', methods=['POST'])
@auth.login_required()
def mod_permission(permissionid):
    session = get_session()
    request_json = request.get_json()

    if not request_json:
        abort(400)

    try:
        permission = session.query(Permission).filter(Permission.id == permissionid).one()

        if 'name' in request_json.keys():
            permission.name = request_json['name']

        if 'description' in request_json.keys():
            permission.password = request_json['description']

        session.commit()
        return "", 200
    except Exception as ex:
        abort(500)


@permission_bp.route(f'/<int:permissionid>', methods=['DELETE'])
@auth.login_required()
def del_permission(permissionid):
    session = get_session()

    try:
        session.query(Permission).filter(Permission.id == permissionid).delete(synchronize_session=False)
        session.commit()
        return "", 200
    except Exception as ex:
        abort(500)


@permission_bp.route(f'/<int:permissionid>', methods=['GET'])
@auth.login_required()
def get_permission(permissionid):
    session = get_session()

    try:
        permission = session.query(Permission).filter(Permission.id == permissionid).one()
        return jsonify(permission)
    except NoResultFound as notfound:
        abort(404)


@permission_bp.route(f'', methods=['GET'])
@auth.login_required(role=['admin', 'corp-owner'])
def get_all_permissions_with_filter():
    session = get_session()

    role = request.args.get('role', default=None, type=str)

    query = session.query(Permission)

    if role:
        query = query.filter(Permission.role.like(f'%{role}%'))

    return jsonify(query.all())
