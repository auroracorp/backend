from flask_httpauth import HTTPTokenAuth

token_auth = HTTPTokenAuth(header='x-api-token')


@token_auth.verify_token
def verify_token(token):
    return True


def get_auth():
    return token_auth
