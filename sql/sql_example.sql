create table corporations
(
    id          INTEGER
        constraint corporation_pk
            primary key autoincrement,
    name        TEXT not null,
    description TEXT
);

create index corporation_name_index
    on corporations (name);

create table permissions
(
    id          INTEGER
        constraint permissions_pk
            primary key autoincrement,
    role        TEXT not null,
    description int
);

create table users
(
    id       INTEGER
        constraint users_pk
            primary key autoincrement,
    name     TEXT not null,
    password TEXT not null
);

create index users_name_index
    on users (name);

create unique index users_password_uindex
    on users (password);

create table users_corporations
(
    id        INTEGER
        constraint user_corportation_pk
            primary key autoincrement,
    corp_id   INTEGER not null
        references corporations,
    user_id   INTEGER not null
        references users,
    corp_role TEXT    not null
);

create table users_permissions
(
    id            INTEGER
        constraint users_permissions_pk
            primary key autoincrement,
    user_id       INTEGER not null
        references users,
    permission_id INTEGER not null
        references permissions
);

